const express = require("express");
const userRoute = express.Router();

// import commonjs Module
const { getUser, getUsers } = require("../controllers/userController");
// tao API voi phuong thuc GET
userRoute.get("/getUser/:id", getUser);
userRoute.get("/getUser", getUsers);
module.exports = userRoute;

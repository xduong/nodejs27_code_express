const User = require("../models/user");

const getUser = async (req, res) => {
  let { id } = req.params;
  console.log(id);
  // SELECT * FROM user WHERE user_id = req.param.id
  let data = await User.findAll({
    where: {
      user_id: id,
    },
  }); // list object [{}]
  // let dataOne = await User.findOne(); // object {}

  res.send(data);
};

const getUsers = async (req, res) => {
  let data = await User.findAll(); // list object [{}]
  res.send(data);
};

const createUser = (req, res) => {
  res.send("Create user");
};

module.exports = {
  getUser,
  createUser,
  getUsers,
};

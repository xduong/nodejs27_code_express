// commonjs module
const express = require("express");

// gắn hàm vàl biến
const app = express();
// cho phep server be doc duoc file json -> day la middware
app.use(express.json());

const cors = require("cors");
const userRouter = require("./routes/userRoutes");
app.use(cors());

// khởi tạo sever
// port: địa chỉ định danh sever
app.listen(8080);

const rootRoute = require("./routes/rootRoutes");

app.use("/api", rootRoute);

console.log("Hello");

// // rest param: truyền biến khhông giới hạn

// // khởi tạo phương thức cho FE
// app.get("/demo", (req, res) => {
//   // nhận dữ liệu FE
//   // lấy dữ liệu từ param -> dùng khi param truyền 1 tham số
//   // /demo/:id/:name -> thường chỉ truyền 1 tham số nếu có 2 thì truyền bằng body
//   //   let id = req.params.id;

//   // lấy dữ liệu từ body -> dùng khi param truyền nhiều thông số (>=2)
//   let { id, hoTen, age } = req.body;

//   console.log({ id }, { hoTen }, { age });
//   // láy dữ liệu tù query -> hạn chế dùng do lộ tên biết
//   // /demo?id=3&&name=node27

//   // lấy dữ liệu từ header
//   let header = req.headers;
//   console.log("header: ", header);

//   // trả dữ liệu về FE
//   // kiểu object, list object, string, boolean nhưng không được là number do trùng status code
//   res.status(200).send({ id, hoTen, age });
// });

// // setup conect to database
// const mysql = require("mysql2");
// const conn = mysql.createConnection({
//   host: "localhost",
//   user: "root",
//   password: "1234",
//   port: 3306,
//   database: "db_node27",
// });

// app.get("/users/:name", (req, res) => {
//   const { name } = req.params;
//   const sql = `SELECT * FROM db_node27.users WHERE users.full_name LIKE "%${name}%"`;

//   conn.query(sql, (err, result) => {
//     res.send(result);
//   });
// });
